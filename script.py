import os
import subprocess
import re
import sys
import requests

import requests

def get_last_commit_message():
    """
    Retrieve the title of the last commit using GitLab API.
    """
    repo_url = "https://gitlab.com/api/v4/projects/berry.othmane%2Fecom-backend-app/repository/commits/"
    try:
        response = requests.get(repo_url)
        response.raise_for_status()
        last_commit_message = response.json()[0]['title']
        return last_commit_message
    except requests.exceptions.RequestException as e:
        print("Error retrieving the last commit title:", e)
        sys.exit(1)




def detect_change_type(commit_message):
    """
    Detect the type of change based on the commit message.
    """
    if re.search(r"BREAKING CHANGE", commit_message, re.IGNORECASE):
        print(f"this is the commit_message: {commit_message}")
        return "major"
    elif re.search(r"feat", commit_message, re.IGNORECASE):
        return "minor"
    else:
        return "patch"

def increment_version(version, tag):
    """
    Increment the version based on the tag provided.
    """
    major, minor, patch = map(int, tag.split('.'))
    if version == 'major':
        major += 1
        minor=0
        patch=0
    elif version == 'minor':
        minor += 1
        patch=0
    elif version == 'patch':
        patch += 1
    else:
        raise ValueError("Invalid tag. Please provide 'major', 'minor', or 'patch'.")
    return f"{major}.{minor}.{patch}"

def get_latest_tag(image_name):
    """
    Retrisseve the latest tag of the image with the specified name from the GitLab Container Registry.
    """
    try:
        # URL to fetch the data
        url = "https://gitlab.com/api/v4/projects/berry.othmane%2Fecom-backend-app/repository/?tags=true"

        # Make the HTTP GET request to fetch the data
        response = requests.get(url)

        # Check if the request was successful (status code 200)
        if response.status_code == 200:
            # Parse the JSON response
            data = response.json()

            # Iterate over the data to find the entry with the matching image name
            for entry in data:
                if entry["name"] == image_name:
                    # Extract tags for the matching image entry
                    tags = [tag["name"] for tag in entry.get("tags", [])]
                    if tags:
                        # Determine the latest tag based on semantic versioning
                        latest_tag = max(tags, key=lambda x: tuple(map(int, x.split('.'))))
                        return latest_tag
                    else:
                        print(f"No tags found for the image '{image_name}'.")
                        return None

            # If no image with the specified name was found
            print(f"Image '{image_name}' not found in the provided data.")
            return None
        else:
            print(f"Failed to fetch data from the URL. Status code: {response.status_code}")
            return None
    except requests.RequestException as e:
        print(f"Error retrieving data from the URL: {e}")
        return None

def create_and_push_image(image_name, tag,personal_access_token):
    try:
# Execute the docker login command
        print("ss")
        username = 'berry.othmane'
        login_command = f"echo {personal_access_token} | docker login registry.gitlab.com/berry.othmane/ecom-backend-app -u {username} --password-stdin"
        os.system(login_command)
        build_command = f"docker build -t registry.gitlab.com/berry.othmane/ecom-backend-app/{image_name.lower()}:{tag} ."
        os.system(build_command)
        trivy_command = f"trivy image registry.gitlab.com/berry.othmane/ecom-backend-app/{image_name.lower()}:{tag}"
        os.system(trivy_command)
        push_command = f"docker push registry.gitlab.com/berry.othmane/ecom-backend-app/{image_name.lower()}:{tag}"
        os.system(push_command)
        print(f"New image registry.gitlab.com/{image_name.lower()}:{tag} created and successfully pushed to the GitLab Container Registry.")
    except subprocess.CalledProcessError as e:
        print(f"Error creating and pushing the new image: {e}")
        sys.exit(1)

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python script.py image_name  personal_access_token")
        sys.exit(1)

    image_name = sys.argv[1]
    personal_access_token = sys.argv[2]


    # Get the last commit message
    last_commit_message = get_last_commit_message()

    # Detect the type of change
    change_type = detect_change_type(last_commit_message)


    # Retrieve the latest tag of the image from the GitLab Container Registry
    latest_tag = get_latest_tag(image_name)

    if latest_tag:
        # Increment the latest tag
        new_tag = increment_version(change_type,latest_tag)
    else:
        print(f"The image {image_name} does not exist on the GitLab Container Registry. Creating the image with tag 1.3.1...")
        new_tag = "1.3.1"

    # Create and push the new image to the GitLab Container Registry
    create_and_push_image(image_name, new_tag,personal_access_token)
